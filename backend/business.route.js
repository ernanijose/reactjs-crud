const express = require('express');
const businessRoutes = express.Router();

// Requisição para a model
let Business = require('./api/business.model');

// Definindo a rota para inclusão
businessRoutes.route('/add').post(function (req, res){
    let business = new Business(req.body);
    business.save().then(business => {
        res.status(200).json({'business': 'business in added succesfully!!'});
    })
    .catch(err => {
        res.status(400).send('Banco de dados indisponivel para salvar');
    });
});

// Definindo a rota para pegar todos os dados
businessRoutes.route('/').get(function(req, res){
    Business.find(function(err, businesses){
        if(err){
            console.log(err);
        }else{
            res.json(businesses);
        }
    });
});

// Definindo a rota para visualização da edição
businessRoutes.route('/edit/:id').get(function(req, res){
    let id = req.params.id;
    Business.findById(id, function(err, business){
        if(err){
            console.log(err);
        }else{
            res.json(business);
        }
    });
});

// Definindo a rota para o update dos dados
businessRoutes.route('/update/:id').post(function(req, res){
    Business.findByOne(req.params.id, function(err, business){
        if(!business){
            res.status(404).send('Dados não encontrado!');
        }else{
            business.person_name = req.body.person_name;
            business.business_name = req.body.business_name;
            business.business_gst_number = req.body.business_gst_number;

            business.save().then(business => {
                res.json("Dados atualizado com sucesso!!!");
            })
            .catch(err => {
                res.status(400).send("Banco de dados indisponivel para atualização");
            });
        }
    });
});

// Definindo rota para exclusão do dado
businessRoutes.route('/delete/:id').get(function(req, res){
    Business.findByIdAndRemove({_id: req.params.id}, function(err, business){
        if(err) res.json(err);
        else res.json("Dado removido com sucesoo!!!");
    });
});

module.exports = businessRoutes;